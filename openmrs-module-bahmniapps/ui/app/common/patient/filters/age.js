'use strict';

angular.module('bahmni.common.patient')
.filter('age', function () {
    return function (age) {
        if (age.years) {
            return age.years + " years";
        }
        if (age.months) {
            return age.months + " months";
        }
        return age.days + " days";
    };
});
